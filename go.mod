module go.jolheiser.com/slashman

go 1.16

require (
	github.com/AlecAivazis/survey/v2 v2.2.12 // indirect
	github.com/peterbourgon/ff/v3 v3.0.0 // indirect
	go.jolheiser.com/beaver v1.1.1 // indirect
	go.jolheiser.com/disco v0.1.3 // indirect
)
