package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/peterbourgon/ff/v3"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/disco/slash"
)

func main() {
	fs := flag.NewFlagSet("slashman", flag.ExitOnError)
	clientID := fs.String("client-id", "", "Application Client ID")
	clientSecret := fs.String("client-secret", "", "Application Client Secret")
	guildID := fs.String("guild-id", "", "Optional Guild ID")
	if err := ff.Parse(fs, os.Args[1:]); err != nil {
		beaver.Fatal(err)
	}

	if err := manage(*clientID, *clientSecret, *guildID); err != nil {
		beaver.Error(err)
	}
}

func manage(clientID, clientSecret, guildID string) error {
	client := slash.NewClient(clientID, clientSecret)

	var opts []string
	optMap := make(map[string]*slash.ApplicationCommand)

	globalCommands, err := client.GetGlobalApplicationCommands(context.Background())
	if err != nil {
		return err
	}
	for _, cmd := range globalCommands {
		name := fmt.Sprintf("%s (global)", cmd.Name)
		opts = append(opts, name)
		optMap[name] = cmd
	}

	if guildID != "" {
		guildCommands, err := client.GetGuildApplicationCommands(context.Background(), guildID)
		if err != nil {
			return err
		}
		for _, cmd := range guildCommands {
			name := fmt.Sprintf("%s (guild)", cmd.Name)
			opts = append(opts, name)
			optMap[name] = cmd
		}
	}

	prompt := &survey.MultiSelect{
		Message: "Commands to remove",
		Options: opts,
	}
	var rm []string
	if err := survey.AskOne(prompt, &rm); err != nil {
		return err
	}

	for _, r := range rm {
		cmd := optMap[r]
		if strings.HasSuffix(r, "(global)") {
			if err := client.DeleteGlobalApplicationCommand(context.Background(), cmd.ID); err != nil {
				beaver.Errorf("could not delete global command '%s': %v", cmd.Name, err)
			}
			continue
		}
		if err := client.DeleteGuildApplicationCommand(context.Background(), guildID, cmd.ID); err != nil {
			beaver.Errorf("could not delete guild command '%s': %v", cmd.Name, err)
		}
	}

	return nil
}
